# Group: group 149

# Question
==========

RQ: Is there a correlation between the Radius (R/Ro) of the star and the Luminosity (L/Lo)?

Null hypothesis: No, there is no correlation between the Radius (R/Ro) of the star and the Luminosity (L/Lo). 

Alternative hypothesis: Yes, there is a correlation between the Radius (R/Ro) of the star and the Luminosity (L/Lo).

# Dataset
==========

URL: https://www.kaggle.com/deepu1109/star-dataset

## Column Headings

```

    > star_dataset <- read.csv("star_dataset.csv")
    > colnames(star_dataset)

[1] "Temperature (K)"        "Luminosity(L/Lo)"      
[3] "Radius(R/Ro)"           "Absolute magnitude(Mv)"
[5] "Star type"              "Star color"            
[7] "Spectral Class"

```